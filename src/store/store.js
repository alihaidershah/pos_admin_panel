import { configureStore, combineReducers } from '@reduxjs/toolkit'
import loaderReducer from '../features/loader/loaderSlice'
import userRoleSlice from '../features/userRole/userRoleSlice'
import tokenSlice from '../features/token/tokenSlice'
import storage from 'redux-persist/lib/storage';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

const persistConfig = {
  key: 'root',
  storage,
}

const rootStore = combineReducers({
    loader: loaderReducer,
    role: userRoleSlice,
    token: tokenSlice,
})

const persistedReducer = persistReducer(persistConfig, rootStore)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk]
})

export const persistor = persistStore(store)