export const API_USER = {
    create: { method: 'post', url: '/user/create', showLoader: true },
    login: { method: 'post', url: '/user/login' },
    update: { method: 'put', url: '/user/update/', showLoader: true },
    delete: { method: 'delete', url: '/user/delete/', showLoader: true },
    fetchAllUsers: { method: 'post', url: '/user/fetch' },
}

export const API_ROLE = {
    fetchAllRoles: { method: 'get', url: '/role/fetch' }
}

export const API_DASHBOARD = {
    getStatistics: { method: 'get', url: '/dashboard/get-statistics',}
}

export const API_ORDER = {
    fetchAllOrders: { method: 'post', url: '/order/fetch' },
    create: { method: 'post', url: 'order/create', showLoader: true },
    update: { method: 'put', url: '/order/update/', showLoader: true },
    delete: { method: 'delete', url: '/order/delete/', showLoader: true },
}


export const API_TAX = {
    fetchAllTaxes: { method: 'post', url: '/tax/fetch' },
    create: { method: 'post', url: 'tax/create', showLoader: true },
    update: { method: 'put', url: '/tax/update/', showLoader: true },
    delete: { method: 'delete', url: '/tax/delete/', showLoader: true },
}

export const API_PAYMENT = {
    create: { method: 'post', url: '/payment/create', showLoader: true }
}

export const API_BRANCH = {
    fetchAllBranches: { method: 'get', url: '/branch/fetch/' },
    fetchAvailableCashiers: { method: 'post', url: '/branch/availableCashiers' },
    fetchAvailableManagers: { method: 'post', url: '/branch/availableManagers' },
    create: { method: 'post', url: '/branch/create', showLoader: true },
    update: { method: 'put', url: '/branch/update/', showLoader: true },
    delete: { method: 'delete', url: '/branch/delete/', showLoader: true },
    updateInventory: { method: 'put', url: '/branch/updateInventory', showLoader: true }
}
// export const API_INVENTORY = {
//     fetchAllBranches: { method: 'get', url: '/branch/fetch' },
//     create: { method: 'post', url: '/branch/create' },
//     update: { method: 'put', url: '/branch/update/' },
//     delete: { method: 'delete', url: '/branch/delete/' },
// }
export const API_PRODUCT = {
    fetchAllProducts: { method: 'get', url: '/product/fetch' },
    create: { method: 'post', url: '/product/create', showLoader: true },
    update: { method: 'put', url: '/product/update/', showLoader: true },
    delete: { method: 'delete', url: '/product/delete/', showLoader: true },
}