// import React from 'react';

export const API_STATUS = {
    unAuthCode: 'E_UNAUTHORIZED',
    ok: 'OK',
    created: 'CREATED',
    un_processable: 'UNPROCESSABLE_ENTITY',
    badRequest: 'BAD_REQUEST'
};
export const API_URL = "http://localhost:4002/api/v1/";
export const MEDIA_URL = API_URL;
export const DEFAULT_API_ERROR = "Something went wrong. Please try again."
export const DEFAULT_SUCCESS_MESSAGE = "Success"

export const USER_TYPE = {
    ADMIN:1,
    CLIENT:2,
    MANAGER:3,
    CASHIER:4
}


export const USER_TYPE_TAB = [
    { id: 1, name: 'Role Admin' },
    { id: 2, name: 'Role Client' },
    { id: 3, name: 'Role Manager' },
    { id: 4, name: 'Role Cashier' },
]

export const ORDER_TYPE_TAB = [
    { id: 1, name: 'Pending',value:"PENDING" },
    { id: 2, name: 'Approved',value:"APPROVED" },
]