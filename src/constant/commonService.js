import moment from 'moment';
const CommonService = {
    displayDate: (date, hour24 = false) => {
        let timeStr = "hh:mm a";
        if (hour24) {
            timeStr = "HH:mm";
        }
        return date ? moment(date).format(`L , ${timeStr}`) : "-";
    },
    getAdviceTime: (time) => {
        let h = Math.floor(time / 60)
        h ? h += " hours" : h = ""
        let m = time - (h * 24)
        m ? m += " minutes" : m = ""
        let str = h + " " + m
        return str
    },
    getDate: (date) => {
        return date ? moment(date).format(`DD/MM/YYYY`) : "";
    },
    getFullDate: (date) => {
        return date ? moment(date).format(`LL`) : "";
    },
    displaySlot: (start, end) => {
        let timeStr = "hh:mm a"
        let startTime = start ? moment(start).format(timeStr) : ""
        let endTime = end ? moment(end).format(timeStr) : ""
        return startTime + " - " + endTime
    },
    displayRangeDate: (start, end) => {
        let startTime = start ? moment(start).format(`DD/MM/YYYY hh:mm a`) : ""
        let endTime = end ? moment(end).format(`DD/MM/YYYY hh:mm a`) : ""
        return startTime + " - " + endTime
    },
    getTime: (date, hour24 = false) => {
        let timeStr = "hh:mm a";
        if (hour24) {
            timeStr = "HH:mm";
        }
        return date ? moment(date).format(`${timeStr}`) : "";
    },
}
export default CommonService