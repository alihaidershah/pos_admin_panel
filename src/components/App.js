import react from 'react'
import { Outlet } from "react-router-dom";
// import Footer from "./Footer";
import Navbar from "./Navbar/Navbar"
import Loader from './Loader'
import { useState } from "react";
import { useSelector } from 'react-redux'

function App() {
    const loaderStatus = useSelector((state) => state.loader.status)
    return (
        <>
            <Navbar />
            {loaderStatus ?
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '70vh' }}>
                    <Loader />
                </div> :
                <Outlet />
            }
            {/* <Dashboard /> */}
            {/* <CreateUser /> */}
            {/* <Index /> */}
            {/* <Footer /> */}
        </>
    )
}
export default App;
