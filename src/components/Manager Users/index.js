import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_USER, API_ROLE, API_BRANCH } from '../../constant/apiConstants';
import { USER_TYPE_TAB } from '../../constant/common'
import { toast } from 'react-toastify';
import CommonService from '../../constant/commonService';
import { Button, Modal, Nav } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { USER_TYPE } from '../../constant/common'
import {
    useSearchParams,
} from "react-router-dom";
import { useSelector } from 'react-redux';

const Index = () => {
    const role = useSelector((state) => state.role.role)
    const [editUser, setEditUser] = useState({})
    const { register: edit, handleSubmit: handleEditSubmit, reset: resetEdit, formState: { errors: editErrors } } = useForm({ mode: 'onChange', defaultValues: editUser });
    const [users, setUsers] = useState([])
    const [showPassword, setShowPassword] = useState(false)
    const { register, handleSubmit: handleRegisterSubmit, reset: resetRegister, formState: { errors: registerErrors } } = useForm({ mode: 'onChange' });
    const [addUser, setAddUser] = useState(false)
    let [filterByRoleId, setfilterByRoleId] = useState(USER_TYPE_TAB[0].id)
    const [showEditModel, setShowEditModel] = useState(false)
    const [reloadUsers, setReloadUsers] = useState(false)
    let [userRole, setUserRole] = useState([])
    let [searchParams, setSearchParams] = useSearchParams();


    useEffect(() => {
        //API call
        let request = { ...API_ROLE.fetchAllRoles };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setUserRole(res.data)
            }
        })
    }, [])

    useEffect(() => {
        setSearchParams({});
        //API call
        let request = {}
        if (role.id == USER_TYPE.ADMIN) {
            request = { ...API_USER.fetchAllUsers };
            request.obj = {
                filter: {
                    'role.id': filterByRoleId
                }
            }
        }
        else if (role.id == USER_TYPE.MANAGER) {
            request = { ...API_BRANCH.fetchAvailableCashiers };
        }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setUsers(res.data)
            }
        });
    }, [reloadUsers, filterByRoleId])

    const onEditSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_USER.update, url: API_USER.update.url + editUser._id };
            request.obj = { ...data };
            delete request.obj._id
            UtilService.callApi(request, function (res) {
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadUsers(v => !v)
                    setShowEditModel(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const onRegisterSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_USER.create };
            request.obj = { ...data };
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    resetRegister()
                    toast.success(res.msg)
                    setReloadUsers(v => !v)
                    setAddUser(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const deleteUser = (id, name) => {
        if (window.confirm(`Are you sure you want to Delete ${name} ?`)) {
            let request = { ...API_USER.delete, url: API_USER.delete.url + id }
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    toast.success(res.msg)
                    setReloadUsers(v => !v)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            })
        }
    }

    useEffect(() => {
        resetEdit(editUser)
    }, [editUser, resetEdit])

    const handleSelect = (eventKey) => {
        setfilterByRoleId(eventKey)
    }
    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h3 className="title">
                        {role.id == USER_TYPE.ADMIN ? 'All Users' : role.id == USER_TYPE.MANAGER ? 'All Available Cashiers' : null}
                    </h3>
                    <button className="button light" onClick={() => { setAddUser(true) }}>
                        <span className="icon"><i className="mdi mdi-pencil"></i></span>
                        <span>{role.id == USER_TYPE.ADMIN ? 'Create User' : role.id == USER_TYPE.MANAGER ? 'Create Cashier' : null}</span>
                    </button>
                </div>
            </section>

            <section className="is-hero-bar">
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h5 className="title">
                    {role.id == USER_TYPE.ADMIN ? 'Search User By Name' : role.id == USER_TYPE.MANAGER ? 'Search Cashier By Name' : null}
                    </h5>
                    <div className="control icons-left icons-right">
                        <input className="input" type="text" name="email" value={searchParams.get("filter") || ""}
                            onChange={(event) => {
                                let filter = event.target.value;
                                if (filter) {
                                    setSearchParams({ filter });
                                } else {
                                    setSearchParams({});
                                }
                            }} />
                        <span className="icon is-small left"><i className="mdi mdi-magnify "></i></span>
                        {searchParams.get("filter") ? <span style={{ cursor: "pointer" }} onClick={() => { setSearchParams({}); }} className="icon is-small right"><i className="mdi mdi-close"></i></span> : null}
                    </div>
                </div>
            </section>
            {role.id == USER_TYPE.ADMIN ?
                <Nav justify variant="tabs" onSelect={handleSelect} activeKey={filterByRoleId}>
                    {USER_TYPE_TAB.map((role) => (
                        <Nav.Item key={role.id}>
                            <Nav.Link eventKey={role.id}>{role.name}</Nav.Link>
                        </Nav.Item>
                    ))}
                </Nav>
                : role.id == USER_TYPE.MANAGER ? null : null}
            <div className="card has-table" style={{ padding: '15px' }}>
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>{users
                            .filter((invoice) => {
                                let filter = searchParams.get("filter");
                                if (!filter) return true;
                                let name = invoice.name.toLowerCase();
                                return name.startsWith(filter.toLowerCase());
                            }).map((user) => (
                                <tr key={user._id}>
                                    <td data-label="Name">{user.name}</td>
                                    <td data-label="Company">{user.email}</td>
                                    <td data-label="City">{user.role.name}</td>
                                    <td data-label="Created">
                                        <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(user.createdAt)}</small>
                                    </td>
                                    <td data-label="Created">
                                        <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(user.updatedAt)}</small>
                                    </td>
                                    <td className="actions-cell">
                                        <div className="buttons right nowrap">
                                            <button className="button small green --jb-modal" onClick={() => { setShowEditModel(true); setEditUser({ _id: user._id, name: user.name, email: user.email, roleId: String(user.role.id) }) }} type="button">
                                                <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                            </button>
                                            <button className="button small red --jb-modal" data-target="sample-modal" onClick={() => { deleteUser(user._id, user.name) }} type="button">
                                                <span className="icon"><i className="mdi mdi-trash-can"></i></span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <Modal show={showEditModel} onHide={() => { setShowEditModel(false); }}>
                <Modal.Header closeButton>
                    <Modal.Title>{role.id == USER_TYPE.ADMIN ? 'Edit User' : role.id == USER_TYPE.MANAGER ? 'Edit Cashier' : null}</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...edit("name", {
                                            required: 'Name is required',
                                        })} />
                                    {editErrors.name && <p className="help">{editErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Email</label>
                            <div className="field">
                                <div className="control icons-left icons-right">
                                    <input className="input" type="text" placeholder="Email"
                                        {...edit("email", {
                                            required: 'Email is required',
                                            pattern: {
                                                value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                message: 'Please enter a valid email',
                                            },
                                        })} />
                                    {editErrors.email && <p className="help">{editErrors.email.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-mail"></i></span>
                                    <span className="icon right"><i className={editErrors.email ? "mdi mdi-close" : "mdi mdi-check"}></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="field" style={{ display: role.id == USER_TYPE.ADMIN ? '' : role.id == USER_TYPE.MANAGER ? 'none' : '' }}>
                        <label className="label">User Role</label>
                        <div className="control">
                            <div className="select">
                                <select {...edit("roleId")}>
                                    {userRole.map((role) => (
                                        <option key={role.id} value={role.id}>{role.name}</option>
                                    ))}
                                </select>
                            </div>
                            {editErrors.roleId && <p className="help">{editErrors.roleId.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                        {role.id == USER_TYPE.ADMIN ? 'Edit User' : role.id == USER_TYPE.MANAGER ? 'Edit Cashier' : null}
                        </Button>
                        <Button variant="danger" onClick={() => { resetEdit(editUser) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            <Modal show={addUser} onHide={() => { setAddUser(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>{role.id == USER_TYPE.ADMIN ? 'Create User' : role.id == USER_TYPE.MANAGER ? 'Create Cashier' : null}</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleRegisterSubmit(onRegisterSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...register("name", {
                                            required: 'Name is required',
                                        })} />
                                    {registerErrors.name && <p className="help">{registerErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Email</label>
                            <div className="field">
                                <div className="control icons-left icons-right">
                                    <input className="input" type="text" placeholder="Email"
                                        {...register("email", {
                                            required: 'Email is required',
                                            pattern: {
                                                value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                message: 'Please enter a valid email',
                                            },
                                        })} />
                                    {registerErrors.email && <p className="help">{registerErrors.email.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-mail"></i></span>
                                    <span className="icon right"><i className={registerErrors.email ? "mdi mdi-close" : "mdi mdi-check"}></i></span>
                                </div>
                            </div>
                            <label className="label">Password</label>
                            <div className="field">
                                <div className="control icons-left icons-right">
                                    <input className="input" type={showPassword ? 'text' : 'password'} placeholder="Enter Your Password"
                                        {...register("password", {
                                            required: { value: true, message: 'Password is required' },
                                            minLength: {
                                                value: 5,
                                                message: "Password Must Be Greater Than Five Character"
                                            }
                                        })} />
                                    {registerErrors.password && <p className="help">{registerErrors.password.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-lock"></i></span>
                                    <span className="icon right" onClick={() => { setShowPassword(value => !value) }}><i className={showPassword ? "mdi mdi-eye-outline " : "mdi mdi-eye-off-outline "}></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="field" style={{ display: role.id == USER_TYPE.ADMIN ? '' : role.id == USER_TYPE.MANAGER ? 'none' : '' }}>
                        <label className="label">User Role</label>
                        <div className="control">
                            <div className="select">
                                <select {...register("roleId")}>
                                    {userRole.map((role) => (
                                        <option key={role.id} value={role.id}>{role.name}</option>
                                    ))}
                                </select>
                            </div>
                            {registerErrors.roleId && <p className="help">{registerErrors.roleId.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                        {role.id == USER_TYPE.ADMIN ? 'Create User' : role.id == USER_TYPE.MANAGER ? 'Create Cashier' : null}
                        </Button>
                        <Button variant="danger" onClick={() => { resetRegister() }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
};

export default Index;