import { React, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import UtilService from '../../services/common.service';
import { API_ROLE, API_USER } from '../../constant/apiConstants';
import { toast } from 'react-toastify';
import Navbar from "../Navbar"

function CreateUser(props) {
    let [userRole, setUserRole] = useState([])
    const [showPassword, setShowPassword] = useState(false)
    const { register, handleSubmit, reset, formState: { errors } } = useForm();

    useEffect(() => {
        //API call
        let request = { ...API_ROLE.fetchAllRoles };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setUserRole(res.data)
            }
        });
    }, [])
    const onSubmit = data => {
        console.log("form data:", data)
        if (data !== '') {
            //API call
            let request = { ...API_USER.create };
            request.obj = { ...data };
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    reset()
                    toast.success(res.msg)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    return (
        <>
        <Navbar />
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h1 className="title">
                        Create User
                    </h1>
                </div>
            </section>
            <div className="card mb-6">
                <div className="card-content">
                    <form onSubmit={handleSubmit(onSubmit)} >
                        <div className="field">
                            <label className="label">Name</label>
                            <div className="field-body">
                                <div className="field">
                                    <div className="control icons-left">
                                        <input className="input" type="text" placeholder="Name"
                                            {...register("name", {
                                                required: 'Name is required',
                                            })} />
                                        {errors.name && <p className="help">{errors.name.message}</p>}
                                        <span className="icon left"><i className="mdi mdi-account"></i></span>
                                    </div>
                                </div>
                                <label className="label">Email</label>
                                <div className="field">
                                    <div className="control icons-left icons-right">
                                        <input className="input" type="text" placeholder="Email"
                                            {...register("email", {
                                                required: 'Email is required',
                                                pattern: {
                                                    value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                    message: 'Please enter a valid email',
                                                },
                                            })} />
                                        {errors.email && <p className="help">{errors.email.message}</p>}
                                        <span className="icon left"><i className="mdi mdi-mail"></i></span>
                                        <span className="icon right"><i className={errors.email ? "mdi mdi-close" : "mdi mdi-check"}></i></span>
                                    </div>
                                </div>
                                <label className="label">Password</label>
                                <div className="field">
                                    <div className="control icons-left icons-right">
                                        <input className="input" type={showPassword ? 'text' : 'password'} placeholder="Enter Your Password"
                                            {...register("password", {
                                                required: { value: true, message: 'Password is required' },
                                                minLength: {
                                                    value: 5,
                                                    message: "Password Must Be Greater Than Five Character"
                                                }
                                            })} />
                                        {errors.password && <p className="help">{errors.password.message}</p>}
                                        <span className="icon left"><i className="mdi mdi-lock"></i></span>
                                        <span className="icon right" onClick={() => { setShowPassword(value => !value) }}><i className={showPassword ? "mdi mdi-eye-off-outline " : "mdi mdi-eye-outline "}></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">User Role</label>
                            <div className="control">
                                <div className="select">
                                    <select {...register("roleId")}>
                                        {userRole.map((role) => (
                                            <option key={role.id} value={role.id}>{role.name}</option>
                                        ))}
                                    </select>
                                </div>
                                {errors.roleId && <p className="help">{errors.roleId.message}</p>}
                            </div>
                        </div>
                        <hr />
                        <div className="field grouped">
                            <div className="control">
                                <button type="submit" className="button green">
                                    Create User
                                </button>
                            </div>
                            <div className="control">
                                <button type="reset" onClick={() => { reset() }} className="button red">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >
        </>
    );
}

export default CreateUser;