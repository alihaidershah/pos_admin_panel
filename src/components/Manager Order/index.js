import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_ORDER, API_PRODUCT, API_TAX, API_BRANCH, API_PAYMENT } from '../../constant/apiConstants';
import { toast } from 'react-toastify';
import CommonService from '../../constant/commonService';
import { Button, Modal, Nav, Container, Row, Col } from 'react-bootstrap';
import { useForm, useFieldArray } from "react-hook-form";
import { ORDER_TYPE_TAB, USER_TYPE } from '../../constant/common';
import { useSelector } from 'react-redux'
import jwt from 'jwt-decode'

const Index = () => {
    const role = useSelector((state) => state.role.role)
    const token = useSelector((state) => state.token.token)
    const [products, setProducts] = useState([])
    let [filterByStatus, setFilterByStatus] = useState(ORDER_TYPE_TAB[0].value)
    const [orders, setOrders] = useState([])
    const [taxes, setTaxes] = useState([])
    const [tax, setTax] = useState(taxes.length > 0 ? taxes[0].cash_p : 0)
    const [subTotal, setSubTotal] = useState(0)
    const [editTax, setEditTax] = useState(0)
    const [editSubTotal, setEditSubTotal] = useState(0)
    let paymentMethods = [{ id: 0, value: null, name: "Select Payment Method" }, { id: 1, name: "-----------Cash-----------", value: 'CASH' }, { id: 2, name: "-----------Card-----------", value: "CARD" }]
    const [branches, setBranches] = useState([])
    const [totalAmount, setTotalAmount] = useState(0)
    const [editTotalAmount, setEditTotalAmount] = useState(0)
    const [changePQty, setChangePQty] = useState(0)
    const [changeEPQty, setChangeEPQty] = useState(0)
    const [createOrder, setCreateOrder] = useState(false)
    const [reloadOrder, setReloadOrder] = useState(false)
    const [resetEditForm, setResetEditForm] = useState(false)
    const { register, control: registerControl, handleSubmit: handleRegisterSubmit, reset, setValue, getValues, formState: { errors: registerErrors } } = useForm({
        mode: 'onChange',
    });
    const { fields, append, remove } = useFieldArray(
        {
            control: registerControl,
            name: "products",
        }
    );
    const { register: edit, control: editControl, handleSubmit: handleEditSubmit, reset: editReset, getValues: editGetValues, setValue: editSetValue, formState: { errors: editErrors } }
        = useForm({
            defaultValues: {},
            mode: 'onChange'
        });
    const { fields: editFields, append: editAppend, remove: editRemove } = useFieldArray(
        {
            control: editControl,
            name: "editProducts",
        }
    );

    const [editOrder, setEditOrder] = useState({})
    const [showEditModel, setShowEditModel] = useState(false)
    const [orderPayment, setOrderPayment] = useState({})
    const [showPaymentModel, setShowPaymentModel] = useState(false)

    useEffect(() => {
        if (showEditModel) {
            let total_amount = 0
            editReset({ branchId: editOrder.branchId, paymentMethod: editOrder.paymentMethod, editProducts: editOrder.editProducts })
            editOrder.editProducts.forEach((object) => {
                let selectProduct = products.filter((p) => { return p._id == object.product })
                total_amount += selectProduct[0].price * parseInt(object.quantity)
            })
            setEditTotalAmount(total_amount)
            setEditSubTotal(editOrder.amount)
            setEditTax(editOrder.paymentMethod == 'CASH' ? taxes[0].cash_p : taxes[0].card_p)
        }
    }, [showEditModel, resetEditForm])

    useEffect(() => {
        if (subTotal > 0) {
            setSubTotal(Math.round((tax * totalAmount) / 100) + totalAmount)
        }
    }, [totalAmount])

    useEffect(() => {
        if (editSubTotal > 0) {
            setEditSubTotal(Math.round((editTax * editTotalAmount) / 100) + editTotalAmount)
        }
    }, [editTotalAmount])

    useEffect(() => {
        let total_amount = 0;
        let total_products = (editGetValues("editProducts"))
        // console.log(total_products)
        total_products.forEach((object) => {
            let selectProduct = products.filter((p) => { return p._id == object.product })
            total_amount += selectProduct[0].price * parseInt(object.quantity)
        })
        setEditTotalAmount(total_amount)
    }, [changeEPQty])


    useEffect(() => {
        let total_amount = 0;
        let total_products = (getValues("products"))
        total_products.forEach((object) => {
            let selectProduct = products.filter((p) => { return p._id == object.product })
            total_amount += selectProduct[0].price * parseInt(object.quantity)
        })
        setTotalAmount(total_amount)
    }, [changePQty])

    useEffect(() => {
        //API call
        let request = { ...API_PRODUCT.fetchAllProducts };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setProducts(res.data)
            }
        });
        request = role.id == USER_TYPE.ADMIN ? { ...API_BRANCH.fetchAllBranches } : role.id == USER_TYPE.MANAGER || role.id == USER_TYPE.CASHIER ?
            { ...API_BRANCH.fetchAllBranches, url: API_BRANCH.fetchAllBranches.url + jwt(token).branchId } : null;
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                if (role.id == USER_TYPE.ADMIN) {
                    setBranches(res.data)
                }
                else if (role.id == USER_TYPE.MANAGER || USER_TYPE.CASHIER) {
                    setBranches(Array(res.data))
                }
            }
        });
        request = { ...API_TAX.fetchAllTaxes };
        request.obj = {
            filter: {
                name: 'GST'
            }
        }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setTaxes(res.data)
            }
        });
    }, [])

    useEffect(() => {
        //API call
        let request = { ...API_ORDER.fetchAllOrders };
        request.obj = {
            filter: {
                "status": filterByStatus
            }
        };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setOrders(res.data)
            }
        });
    }, [reloadOrder, filterByStatus])


    const createNewOrder = () => {
        let data = getValues();
        if (!(data.products.length > 0)) {
            alert("Plz Add Products")
            return;
        }
        if (data.paymentMethod == "Select Payment Method") {
            alert("Plz Select Payment Method")
            return;
        }
        //API call
        let request = { ...API_ORDER.create };
        request.obj = { ...data };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                reset()
                toast.success(res.msg)
                setReloadOrder(v => !v)
                setTax(0); setSubTotal(0); setTotalAmount(0);
                setCreateOrder(false)
            }
            else {
                if (res.response) {
                    //   console.log(res)
                    toast.error((res.response.data.msg))
                }
                else {
                    toast.error(res.msg)
                }
            }
        });
    };

    const editNewOrder = () => {
        let data = editGetValues();
        if (!(data.editProducts.length > 0)) {
            alert("Plz Add Products")
            return;
        }
        if (data.paymentMethod == "Select Payment Method") {
            alert("Plz Select Payment Method")
            return;
        }
        //API call
        let request = { ...API_ORDER.update, url: API_ORDER.update.url + editOrder.id };
        request.obj = { branchId: data.branchId, paymentMethod: data.paymentMethod, products: data.editProducts };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                editReset()
                toast.success(res.msg)
                setReloadOrder(v => !v)
                setShowEditModel(false)
            }
            else {
                if (res.response) {
                    //   console.log(res)
                    toast.error((res.response.data.msg))
                }
                else {
                    toast.error(res.msg)
                }
            }
        });
    };

    const onRegisterSubmit = data => {
        return
    };
    const onEditRegisterSubmit = data => {
        return
    };

    const handleSelect = (eventKey) => {
        setOrders([])
        setFilterByStatus(eventKey)
    }

    const deleteOrder = (id, index) => {
        if (window.confirm(`Are you sure you want to Delete #${index + 1} Order ?`)) {
            let request = { ...API_ORDER.delete, url: API_ORDER.delete.url + id }
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    toast.success(res.msg)
                    setReloadOrder(v => !v)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            })
        }
    }
    const payOrder = (id, amount) => {
        let request = { ...API_PAYMENT.create }
        request.obj = { amount, orderId: id }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                toast.success(res.msg)
                setShowPaymentModel(false)
                setReloadOrder(v => !v)
                setOrderPayment({})
            }
            else {
                //   console.log(res.response.data.msg)
                toast.error((res.response.data.msg))
            }
        })
    }

    return (
        <>
            {/* <Navbar /> */}

            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h2 className="title">
                        All Orders
                    </h2>
                    <button className="button light" onClick={() => { setCreateOrder(true) }}>
                        <span className="icon"><i className="mdi mdi-pencil"></i></span>
                        <span>Create Order</span>
                    </button>
                </div>
            </section>

            <Nav justify variant="tabs" onSelect={handleSelect} activeKey={filterByStatus}>
                {ORDER_TYPE_TAB.map((role) => (
                    <Nav.Item key={role.id}>
                        <Nav.Link eventKey={role.value}>{role.name}</Nav.Link>
                    </Nav.Item>
                ))}
            </Nav>

            <div className="card has-table" style={{ padding: '15px' }}>
                {/* <header className="card-header">
                    <p className="card-header-title">
                        <span className="icon"><i className="mdi mdi-account-multiple"></i></span>
                        Users
                    </p>

                </header> */}
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Products</th>
                                <th>Status</th>
                                <th>Branch</th>
                                <th>Total Amount</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                {filterByStatus == "PENDING" ? <th>Actions</th> : null}
                            </tr>
                        </thead>
                        <tbody>
                            {orders.map((order, index) => (
                                <tr key={order._id}>
                                    <td>{index + 1}</td>
                                    <td>{order.products.map((p, index, products) => (<span key={p._id}>{`${p.product.name}:${p.quantity}${(index + 1 === products.length) ? '' : ','}`}</span>))}</td>
                                    <td>{order.status}</td>
                                    <td>{order.branch.name}</td>
                                    <td>{order.total_amount}</td>
                                    <td data-label="Created">
                                        <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(order.createdAt)}</small>
                                    </td>
                                    <td data-label="Created">
                                        <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(order.updatedAt)}</small>
                                    </td>
                                    {filterByStatus === "PENDING" && orders.length > 0 ?
                                        <td className="actions-cell">
                                            <div className="buttons right nowrap">
                                                <button className="button small green --jb-modal" onClick={() => {
                                                    setShowEditModel(true);
                                                    setEditOrder({
                                                        paymentMethod: order.paymentMethod, branchId: order.branch._id, amount: order.total_amount, id: order._id,
                                                        editProducts: order.products.map((obj) => { return { product: obj.product._id, quantity: obj.quantity } })
                                                    })
                                                }} type="button">
                                                    <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                                </button>
                                                {role.id == USER_TYPE.ADMIN || role.id == USER_TYPE.MANAGER ?
                                                    <button className="button small red --jb-modal" data-target="sample-modal" onClick={() => { deleteOrder(order._id, index) }} type="button">
                                                        <span className="icon"><i className="mdi mdi-trash-can"></i></span>
                                                    </button>
                                                    : null}
                                                <button className="button small green --jb-modal" data-target="sample-modal" onClick={() => {
                                                    setShowPaymentModel(true);
                                                    setOrderPayment({
                                                        products: order.products.map((obj) => { return { id: obj._id, price: obj.product.price, name: obj.product.name, quantity: obj.quantity } }),
                                                        paymentMethod: order.paymentMethod, branchId: order.branch._id, amount: order.total_amount, id: order._id,
                                                        editProducts: order.products.map((obj) => { return { product: obj.product._id, quantity: obj.quantity } })
                                                    })
                                                }} type="button">
                                                    <span className="icon"><i className="mdi mdi-cash "></i></span>
                                                </button>
                                            </div>
                                        </td>
                                        : null
                                    }
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <Modal show={createOrder} onHide={() => { setCreateOrder(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Order</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleRegisterSubmit(onRegisterSubmit)} >
                    <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                        <div className="control">
                            <div className="select" style={{ display: role.id == USER_TYPE.ADMIN ? '' : role.id == USER_TYPE.MANAGER || USER_TYPE.CASHIER ? 'none' : '' }}>
                                <select {...register('branchId')}>
                                    {branches.map((b) => (
                                        <option key={b._id} value={b._id}>{b.name}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <button className="button light" onClick={() => {
                            append({ product: products[0].id, quantity: 1 });
                            setChangePQty((p) => p + 1)
                        }}>
                            <span className="icon"><i className="mdi mdi-plus-circle-outline"></i></span>
                        </button>
                    </div>
                    <hr />
                    {fields.map((item, i) => (
                        <Container key={i}>
                            <Row>
                                <Col xs={6}>
                                    <div className="field">
                                        <label className="label">Products</label>
                                        <div className="control">
                                            <div className="select">
                                                <select {...register(`products[${i}].product`)}
                                                    onChange={(e) => { setChangePQty((p => p + 1)); setValue(`products[${i}].product`, e.target.value) }}>
                                                    {products.map((p) => (
                                                        <option key={p._id} value={p._id}>{p.name}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            {/* {registerErrors.name && <p className="help">{registerErrors.name.message}</p>} */}
                                        </div>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="field">
                                        <label className="label">quantity</label>
                                        <div className="control">
                                            <input className="input" type="number"
                                                {...register(`products[${i}].quantity`, { valueAsNumber: true, validate: (value) => value > 1 })}
                                                onChange={(e) => { if (e.target.value >= 1) { setChangePQty((p => p + 1)) }; setValue(`products[${i}].quantity`, e.target.value > 1 ? e.target.value : 1) }} />
                                            {/* {registerErrors.name && <p className="help">{registerErrors.name.message}</p>} */}
                                        </div>
                                    </div>
                                </Col>
                                <Col style={{ paddingTop: "34px" }}>
                                    <button className="button small green --jb-modal" onClick={() => { remove(i); setChangePQty((p) => p + 1) }} type="button">
                                        <span className="icon"><i className="mdi mdi-delete"></i></span>
                                    </button>
                                </Col>
                            </Row>
                        </Container>
                    ))}
                    <hr />
                    <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                        <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                            <h5 className="title">
                                Payment Method
                            </h5>
                            <select {...register('paymentMethod')} onChange={(e) => {
                                if (e.target.value == 'CASH') {
                                    setTax(taxes[0].cash_p)
                                    setSubTotal(Math.round((taxes[0].cash_p * totalAmount) / 100) + totalAmount)
                                }
                                else if (e.target.value == 'CARD') {
                                    setTax(taxes[0].card_p)
                                    setSubTotal(Math.round((taxes[0].card_p * totalAmount) / 100) + totalAmount)
                                }
                                else {
                                    setTax(0)
                                    setSubTotal(0)
                                }
                            }}>
                                {paymentMethods.map((obj) => (
                                    <option key={obj.id} value={obj.value}>{obj.name}</option>
                                ))}
                            </select>
                        </div>
                    </section>
                    <span>Total Amount: {totalAmount}</span>
                    <br />
                    <span>Tax: {tax}%</span>
                    <br />
                    <span>Sub Total: {subTotal}</span>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => { createNewOrder() }}>
                            Create Order
                        </Button>
                        <Button variant="danger" onClick={() => { reset(); setTax(0); setSubTotal(0); setTotalAmount(0); }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>


            {/* Edit Model */}

            <Modal show={showEditModel} onHide={() => { setShowEditModel(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Order</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditRegisterSubmit)} >
                    <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                        <div className="control">
                            <div className="select" style={{ display: role.id == USER_TYPE.ADMIN ? '' : role.id == USER_TYPE.MANAGER || USER_TYPE.CASHIER ? 'none' : '' }}>
                                <select {...edit('branchId')}>
                                    {branches.map((b) => (
                                        <option key={b._id} value={b._id}>{b.name}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <button className="button light" onClick={() => {
                            editAppend({ product: products[1].id, quantity: 1 });
                            setChangeEPQty((p) => p + 1)
                        }}>
                            <span className="icon"><i className="mdi mdi-plus-circle-outline"></i></span>
                        </button>
                    </div>
                    <hr />
                    {editFields.map((item, i) => (
                        <Container key={i}>
                            <Row>
                                <Col xs={6}>
                                    <div className="field">
                                        <label className="label">Products</label>
                                        <div className="control">
                                            <div className="select">
                                                <select {...edit(`editProducts[${i}].product`)}
                                                    onChange={(e) => { setChangeEPQty((p) => p + 1); editSetValue(`editProducts[${i}].product`, e.target.value) }}>
                                                    {products.map((p) => (
                                                        <option key={p._id} value={p._id}>{p.name}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            {/* {registerErrors.name && <p className="help">{registerErrors.name.message}</p>} */}
                                        </div>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="field">
                                        <label className="label">quantity</label>
                                        <div className="control">
                                            <input className="input" type="number"
                                                {...edit(`editProducts[${i}].quantity`, { valueAsNumber: true, validate: (value) => value > 1 })}
                                                onChange={(e) => { if (e.target.value >= 1) { setChangeEPQty((p => p + 1)) }; editSetValue(`editProducts[${i}].quantity`, e.target.value > 1 ? e.target.value : 1) }} />
                                            {/* {registerErrors.name && <p className="help">{registerErrors.name.message}</p>} */}
                                        </div>
                                    </div>
                                </Col>
                                <Col style={{ paddingTop: "34px" }}>
                                    <button className="button small green --jb-modal" onClick={() => { setChangeEPQty((p) => p + 1); editRemove(i) }} type="button">
                                        <span className="icon"><i className="mdi mdi-delete"></i></span>
                                    </button>
                                </Col>
                            </Row>
                        </Container>
                    ))}
                    <hr />
                    <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                        <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                            <h5 className="title">
                                Payment Method
                            </h5>
                            <select {...edit('paymentMethod')} onChange={(e) => {
                                if (e.target.value == 'CASH') {
                                    setEditTax(taxes[0].cash_p)
                                    setEditSubTotal(Math.round((taxes[0].cash_p * editTotalAmount) / 100) + editTotalAmount)
                                }
                                else if (e.target.value == 'CARD') {
                                    setEditTax(taxes[0].card_p)
                                    setEditSubTotal(Math.round((taxes[0].card_p * editTotalAmount) / 100) + editTotalAmount)
                                }
                                else {
                                    setEditTax(0)
                                    setEditSubTotal(0)
                                }
                            }}>
                                {paymentMethods.map((obj) => (
                                    <option key={obj.id} value={obj.value}>{obj.name}</option>
                                ))}
                            </select>
                        </div>
                    </section>
                    <span>Total Amount: {editTotalAmount}</span>
                    <br />
                    <span>Tax: {editTax}%</span>
                    <br />
                    <span>Sub Total: {editSubTotal}</span>
                    <Modal.Footer>
                        <Button variant="primary" onClick={() => { editNewOrder() }}>
                            Edit Order
                        </Button>
                        <Button variant="danger" onClick={() => { setResetEditForm((p) => !p) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            {/* Payment Model */}

            <Modal show={showPaymentModel} onHide={() => { setShowPaymentModel(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Order Payment</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="card-content">
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {orderPayment.products ? orderPayment.products.map((p, index) => (
                                    <tr key={p.id}>
                                        <td>{index + 1}</td>
                                        <td>{p.name}</td>
                                        <td>{p.quantity}</td>
                                        <td>{parseInt(p.quantity) * (parseInt(p.price))}</td>
                                    </tr>
                                )) : null}
                            </tbody>
                        </table>
                        <hr />
                        <div style={{ fontWeight: "bold" }} className='flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0'>
                            <span>Total: {orderPayment.products ?
                                orderPayment.products.reduce((total, obj) => {
                                    if (obj) {
                                        return total + parseInt(obj.quantity) * (parseInt(obj.price));
                                    }
                                    else {
                                        return total
                                    }
                                }, 0)
                                :
                                0}</span>
                            <span>Tax: {orderPayment.products ?
                                orderPayment.amount - orderPayment.products.reduce((total, obj) => {
                                    if (obj) {
                                        return total + parseInt(obj.quantity) * (parseInt(obj.price));
                                    }
                                    else {
                                        return total
                                    }
                                }, 0)
                                :
                                0}</span>
                            <span>Sub Total: {orderPayment.products ? orderPayment.amount : 0}</span>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => {
                        setShowPaymentModel(false)
                        setShowEditModel(true);
                        if (orderPayment.editProducts) {
                            setEditOrder({
                                paymentMethod: orderPayment.paymentMethod, branchId: orderPayment.branch,
                                amount: orderPayment.amount, id: orderPayment.id,
                                editProducts: orderPayment.editProducts
                            })
                        }
                    }} type="submit">
                        Edit Order
                    </Button>
                    <Button variant="primary" onClick={() => { payOrder(orderPayment.id, orderPayment.amount) }} type="submit">
                        Pay
                    </Button>
                    <Button variant="danger" onClick={() => { setShowPaymentModel(false) }}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default Index;