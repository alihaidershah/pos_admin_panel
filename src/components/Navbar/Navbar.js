// import react from 'react'
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { setToken } from '../../features/token/tokenSlice'
import './nav.css'

function Navbar() {
    let navigate = useNavigate();
    const dispatch = useDispatch()
    const role = useSelector((state) => state.role.role)
    let routes = {
        ADMIN: [
            { id: 1, name: "Dashboard", to: "/dashboard", icon: 'mdi-monitor-dashboard' },
            { id: 2, name: "Manage Users", to: "/users", icon: 'mdi-account' },
            { id: 6, name: "Manage Products", to: "/products", icon: 'mdi-archive-outline' },
            { id: 3, name: "Manage Branches", to: "/branches", icon: 'mdi-source-branch' },
            { id: 4, name: "Manage Inventory", to: "/inventory", icon: 'mdi-sticker-plus-outline' },
            { id: 5, name: "Manage Orders", to: "/order", icon: 'mdi-cart-outline' },
            { id: 7, name: "Manage Taxes", to: "/tax", icon: 'mdi-file-percent-outline' },
        ],
        MANAGER: [
            { id: 1, name: "Manage Cashiers", to: "/users", icon: 'mdi-account' },
            { id: 4, name: "Manage Branch", to: "/branches", icon: 'mdi-source-branch' },
            { id: 2, name: "Manage Inventory", to: "/inventory", icon: 'mdi-sticker-plus-outline' },
            { id: 3, name: "Manage Orders", to: "/order", icon: 'mdi-cart-outline' },
            { id: 7, name: "Manage Taxes", to: "/tax", icon: 'mdi-file-percent-outline' },
        ],
        CASHIER: [
            { id: 1, name: "Manage Orders", to: "/order", icon: 'mdi-cart-outline' },
        ]
    }

    return (
        <>
            {/* {console.log("navbar",)} */}
            <aside className="aside is-placed-left is-expanded">
                <div className="aside-tools">
                    <div>
                        {role.name} <b className="font-black">PANEL</b>
                    </div>
                </div>
                <div className="menu is-menu-main">
                    <ul className="menu-list">
                        {routes[role.name].map((route) => (
                            <li className="" key={route.id}>
                                <Link to={route.to} className="links">
                                    <span className="icon"><i className={"mdi " + route.icon}></i></span>
                                    <span className="menu-item-label">{route.name}</span>
                                </Link>
                            </li>
                        ))}
                        <li className="">
                            <a href="#" className="links">
                                <span className="icon"><i className="mdi mdi-desktop-mac"></i></span>
                                <span onClick={() => { dispatch(setToken('')); navigate("/login", { replace: true }); }} className="menu-item-label">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </aside>
        </>
    )
}
export default Navbar;
