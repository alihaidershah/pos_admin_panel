import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_BRANCH, API_USER, API_PRODUCT } from '../../constant/apiConstants';
import { toast } from 'react-toastify';
import CommonService from '../../constant/commonService';
import { Button, Modal } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import {
    useSearchParams,
} from "react-router-dom";

const Index = () => {
    const [editProduct, setEditProduct] = useState({})
    const { register: edit, handleSubmit: handleEditSubmit, reset: resetEdit, formState: { errors: editErrors } } = useForm({ mode: 'onChange', defaultValues: editProduct });
    const [products, setProducts] = useState([])
    const { register, handleSubmit: handleRegisterSubmit, reset: resetRegister, formState: { errors: registerErrors } } = useForm({ mode: 'onChange' });
    const [addBranch, setAddBranch] = useState(false)
    const [showEditModel, setShowEditModel] = useState(false)
    const [reloadproducts, setReloadproducts] = useState(false)
    let [availableManagers, setAvailableManagers] = useState([])
    let [availableCashiers, setAvailableCashiers] = useState([])
    let [searchParams, setSearchParams] = useSearchParams();
    let productCategories = [, { id: 2, value: "Milk" }, { id: 3, value: "Cream" }, { id: 4, value: "Cheese" },
        { id: 5, value: "Butter" }, { id: 6, value: "Margarine" }, { id: 7, value: "Breads" }, { id: 8, value: "Biscuits" }, { id: 9, value: "Cakes" }]

    useEffect(() => {
        //API call
        let request = { ...API_BRANCH.fetchAvailableManagers };
        if (showEditModel) {
            request.obj = { _id: editProduct._id }
        }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setAvailableManagers(res.data)
            }
        })
        request = { ...API_BRANCH.fetchAvailableCashiers };
        if (showEditModel) {
            request.obj = { _id: editProduct._id }
        }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setAvailableCashiers(res.data)
            }
        })
    }, [showEditModel, addBranch])

    useEffect(() => {
        //API call
        setSearchParams({});
        let request = { ...API_PRODUCT.fetchAllProducts };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setProducts(res.data)
            }
        });
    }, [reloadproducts])

    const onEditSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_PRODUCT.update, url: API_PRODUCT.update.url + editProduct._id };
            request.obj = { ...data, categories: String(data.categories) };
            delete request.obj._id
            console.log("edit", request)
            UtilService.callApi(request, function (res) {
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadproducts(v => !v)
                    setShowEditModel(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const onRegisterSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_PRODUCT.create };
            request.obj = { ...data, categories: String(data.categories) };
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    resetRegister()
                    toast.success(res.msg)
                    setReloadproducts(v => !v)
                    setAddBranch(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const deleteProduct = (id, name) => {
        if (window.confirm(`Are you sure you want to Delete ${name} Product ?`)) {
            let request = { ...API_PRODUCT.delete, url: API_PRODUCT.delete.url + id }
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    toast.success(res.msg)
                    setReloadproducts(v => !v)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            })
        }
    }

    useEffect(() => {
        resetEdit(editProduct)
    }, [showEditModel])

    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h2 className="title">
                        All products
                    </h2>
                    <button className="button light" onClick={() => { setAddBranch(true) }}>
                        <span className="icon"><i className="mdi mdi-pencil"></i></span>
                        <span>Create Product</span>
                    </button>
                </div>
            </section>

            <section className="is-hero-bar">
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h5 className="title">
                        Search User By Product Name
                    </h5>
                    <div className="control icons-left icons-right">
                        <input className="input" type="text" name="email" value={searchParams.get("filter") || ""}
                            onChange={(event) => {
                                let filter = event.target.value;
                                if (filter) {
                                    setSearchParams({ filter });
                                } else {
                                    setSearchParams({});
                                }
                            }} />
                        <span className="icon is-small left"><i className="mdi mdi-magnify "></i></span>
                        {searchParams.get("filter") ? <span style={{ cursor: "pointer" }} onClick={() => { setSearchParams({}); }} className="icon is-small right"><i className="mdi mdi-close"></i></span> : null}
                    </div>
                </div>
            </section>

            <div className="card has-table" style={{ padding: '15px' }}>
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Categories</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products
                                .filter((product) => {
                                    let filter = searchParams.get("filter");
                                    if (!filter) return true;
                                    let name = product.name.toLowerCase();
                                    return name.startsWith(filter.toLowerCase());
                                }).map((product) => (
                                    <tr key={product._id}>
                                        <td data-label="Name">{product.name}</td>
                                        <td data-label="Name">{product.price}</td>
                                        {product.categories.length > 0 ? <td>{product.categories.map((catagory, index, catagories) => (<span key={index}>{`${catagory}${(index + 1 === catagories.length) ? '' : ','}`}</span>))}</td> : <td>No Catagory Assign</td>}
                                        <td data-label="Name">{product.description}</td>
                                        <td data-label="Name">{product.status}</td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(product.createdAt)}</small>
                                        </td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(product.updatedAt)}</small>
                                        </td>
                                        <td className="actions-cell">
                                            <div className="buttons right nowrap">
                                                <button className="button small green --jb-modal" onClick={() => { setShowEditModel(true); setEditProduct({ _id: product._id, description: product.description, name: product.name, categories: product.categories, status: product.status, price: product.price }) }} type="button">
                                                    <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                                </button>
                                                <button className="button small red --jb-modal" data-target="sample-modal" onClick={() => { deleteProduct(product._id, product.name) }} type="button">
                                                    <span className="icon"><i className="mdi mdi-trash-can"></i></span>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <Modal show={showEditModel} onHide={() => { setShowEditModel(false); }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Manager</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...edit("name", {
                                            required: 'Name is required',
                                        })} />
                                    {editErrors.name && <p className="help">{editErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Price</label>
                            <div className="field">
                                <div className="control">
                                    <input className="input" type='number' placeholder="Product Price"
                                        {...edit("price", {
                                            required: 'Price is required',
                                            validate: {
                                                positive: v => parseInt(v) > 0 || 'Price should be greater than 0',
                                            }
                                        })} />
                                    {editErrors.price && <p className="help">{editErrors.price.message}</p>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Categories</label>
                        <div className="control">
                            <div className="select">
                                <select {...edit("categories",{
                                    required: 'Categories Is Required'
                                })} multiple>
                                    {productCategories.map((category) => (
                                        <option key={category.id} value={category.value}>{category.value}</option>
                                    ))}
                                </select>
                            </div>
                            {editErrors.categories && <p className="help">{editErrors.categories.message}</p>}
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Status</label>
                        <div className="control">
                            <div className="select">
                                <select {...edit("status")}>
                                    {[{ id: 1, value: "ACTIVE", name: "Active" }, { id: 2, value: "NOT_ACTIVE", name: "Not_Active" }].map((status) => (
                                        <option key={status.id} value={status.value}>{status.name}</option>
                                    ))}
                                </select>
                            </div>
                            {editErrors.status && <p className="help">{editErrors.status.message}</p>}
                        </div>
                    </div>
                    <label className="label">Description</label>
                    <div className="field">
                        <div className="control">
                            <textarea className="input" placeholder="Product Description"
                                {...edit("description", {
                                    required: 'Description is required',
                                })} />
                            {editErrors.description && <p className="help">{editErrors.description.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Edit Branch
                        </Button>
                        <Button variant="danger" onClick={() => { resetEdit(editProduct) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            <Modal show={addBranch} onHide={() => { setAddBranch(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Product</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleRegisterSubmit(onRegisterSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...register("name", {
                                            required: 'Name is required',
                                        })} />
                                    {registerErrors.name && <p className="help">{registerErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Price</label>
                            <div className="field">
                                <div className="control">
                                    <input className="input" type='number' placeholder="Product Price"
                                        {...register("price", {
                                            required: 'Price is required',
                                            validate: {
                                                positive: v => parseInt(v) > 0 || 'Price should be greater than 0',
                                            }
                                        })} />
                                    {registerErrors.price && <p className="help">{registerErrors.price.message}</p>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Categories</label>
                        <div className="control">
                            <div className="select">
                                <select {...register("categories",{
                                    required: 'Categories Is Required'
                                })} multiple>
                                    {productCategories.map((category) => (
                                        <option key={category.id} value={category.value}>{category.value}</option>
                                    ))}
                                </select>
                            </div>
                            {registerErrors.categories && <p className="help">{registerErrors.categories.message}</p>}
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Status</label>
                        <div className="control">
                            <div className="select">
                                <select {...register("status")}>
                                    {[{ id: 1, value: "ACTIVE", name: "Active" }, { id: 2, value: "NOT_ACTIVE", name: "Not_Active" }].map((status) => (
                                        <option key={status.id} value={status.value}>{status.name}</option>
                                    ))}
                                </select>
                            </div>
                            {registerErrors.status && <p className="help">{registerErrors.status.message}</p>}
                        </div>
                    </div>
                    <label className="label">Description</label>
                    <div className="field">
                        <div className="control">
                            <textarea className="input" placeholder="Product Description"
                                {...register("description", {
                                    required: 'Description is required',
                                })} />
                            {registerErrors.description && <p className="help">{registerErrors.description.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Create Product
                        </Button>
                        <Button variant="danger" onClick={() => { resetRegister() }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
};

export default Index;