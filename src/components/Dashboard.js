// import react from 'react
import { useEffect, useState } from 'react';
import UtilService from '../services/common.service';
import { API_DASHBOARD } from "../constant/apiConstants";

function Dashboard() {
    let [statistics, setStatistics] = useState([])

    useEffect(() => {
        //API call
        let request = { ...API_DASHBOARD.getStatistics };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setStatistics(res.data)
            }
        });
    }, [])

    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "20px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h1 className="title">
                        Dashboard
                    </h1>
                </div>
            </section>

            <section className="section main-section">
                <div className="grid gap-6 grid-cols-1 md:grid-cols-3 mb-6">
                    <div className="card">
                        <div className="card-content">
                            <div className="flex items-center justify-between">
                                <div className="widget-label">
                                    <h3>
                                        Clients
                                    </h3>
                                    <h1>
                                        {statistics.clientCount}
                                    </h1>
                                </div>
                                <span className="icon widget-icon text-green-500"><i className="mdi mdi-account-multiple mdi-48px"></i></span>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-content">
                            <div className="flex items-center justify-between">
                                <div className="widget-label">
                                    <h3>
                                        products
                                    </h3>
                                    <h1>
                                    {statistics.productCount}
                                    </h1>
                                </div>
                                <span className="icon widget-icon text-blue-500"><i className="mdi mdi-archive-outline mdi-48px"></i></span>
                            </div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-content">
                            <div className="flex items-center justify-between">
                                <div className="widget-label">
                                    <h3>
                                        Branches
                                    </h3>
                                    <h1>
                                    {statistics.branchCount}
                                    </h1>
                                </div>
                                <span className="icon widget-icon text-red-500"><i className="mdi mdi-source-branch mdi-48px"></i></span>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-content">
                            <div className="flex items-center justify-between">
                                <div className="widget-label">
                                    <h3>
                                        Orders
                                    </h3>
                                    <h1>
                                    {statistics.orderCount}
                                    </h1>
                                </div>
                                <span className="icon widget-icon text-red-500"><i className="mdi  mdi-cart-outline mdi-48px"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </>
    )
}
export default Dashboard;
