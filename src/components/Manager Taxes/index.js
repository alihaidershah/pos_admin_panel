import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_BRANCH, API_TAX } from '../../constant/apiConstants';
import { toast } from 'react-toastify';
import CommonService from '../../constant/commonService';
import { Button, Modal } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux'
import {
    useSearchParams,
} from "react-router-dom";

const Index = () => {
    const [editTax, setEditTax] = useState({})
    const { register: edit, handleSubmit: handleEditSubmit, reset: resetEdit, formState: { errors: editErrors } } = useForm({ mode: 'onChange', defaultValues: editTax });
    const [taxes, setTaxes] = useState([])
    const { register, handleSubmit: handleRegisterSubmit, reset: resetRegister, formState: { errors: registerErrors } } = useForm({ mode: 'onChange',defaultValues:{cash_p:1,card_p:1} });
    const [addTax, setAddTax] = useState(false)
    const [showEditModel, setShowEditModel] = useState(false)
    const [reloadtaxes, setReloadtaxes] = useState(false)
    let [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        //API call
        setSearchParams({});
        let request = { ...API_TAX.fetchAllTaxes }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setTaxes(res.data)
            }
        });
    }, [reloadtaxes])

    const onEditSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_TAX.update, url: API_TAX.update.url + editTax._id };
            request.obj = { ...data };
            delete request.obj._id
            UtilService.callApi(request, function (res) {
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadtaxes(v => !v)
                    setShowEditModel(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const onRegisterSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_TAX.create };
            request.obj = { ...data };
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    resetRegister()
                    toast.success(res.msg)
                    setReloadtaxes(v => !v)
                    setAddTax(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const deleteTax = (id, name) => {
        if (window.confirm(`Are you sure you want to Delete ${name} Tax ?`)) {
            let request = { ...API_TAX.delete, url: API_TAX.delete.url + id }
            UtilService.callApi(request, function (res) {
                console.log(res)
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadtaxes(v => !v)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            })
        }
    }

    useEffect(() => {
        resetEdit(editTax)
    }, [showEditModel])


    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h2 className="title">
                        All Taxes
                    </h2>
                    <button className="button light" onClick={() => { setAddTax(true) }}>
                        <span className="icon"><i className="mdi mdi-pencil"></i></span>
                        <span>Create Tax</span>
                    </button>
                </div>
            </section>

            <section className="is-hero-bar">
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h5 className="title">
                        Search Tax By Name
                    </h5>
                    <div className="control icons-left icons-right">
                        <input className="input" type="text" name="email" value={searchParams.get("filter") || ""}
                            onChange={(event) => {
                                let filter = event.target.value;
                                if (filter) {
                                    setSearchParams({ filter });
                                } else {
                                    setSearchParams({});
                                }
                            }} />
                        <span className="icon is-small left"><i className="mdi mdi-magnify "></i></span>
                        {searchParams.get("filter") ? <span style={{ cursor: "pointer" }} onClick={() => { setSearchParams({}); }} className="icon is-small right"><i className="mdi mdi-close"></i></span> : null}
                    </div>
                </div>
            </section>

            <div className="card has-table" style={{ padding: '15px' }}>
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Card persentage</th>
                                <th>Cash persentage</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {taxes
                                .filter((tax) => {
                                    let filter = searchParams.get("filter");
                                    if (!filter) return true;
                                    let name = tax.name.toLowerCase();
                                    return name.startsWith(filter.toLowerCase());
                                }).map((tax) => (
                                    <tr key={tax._id}>
                                        <td data-label="Name">{tax.name}</td>
                                        <td data-label="Company">{tax.cash_p}</td>
                                        <td data-label="City">{tax.card_p}</td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(tax.createdAt)}</small>
                                        </td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(tax.updatedAt)}</small>
                                        </td>
                                        <td className="actions-cell">
                                            <div className="buttons right nowrap">
                                                <button className="button small green --jb-modal" onClick={() => { setShowEditModel(true); setEditTax({ _id: tax._id, name: tax.name, cash_p: tax.cash_p, card_p: tax.card_p }) }} type="button">
                                                    <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                                </button>
                                                <button className="button small red --jb-modal" data-target="sample-modal" onClick={() => { deleteTax(tax._id, tax.name) }} type="button">
                                                    <span className="icon"><i className="mdi mdi-trash-can"></i></span>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <Modal show={showEditModel} onHide={() => { setShowEditModel(false); }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Tax</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...edit("name", {
                                            required: 'Name is required',
                                        })} />
                                    {editErrors.name && <p className="help">{editErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                        </div>
                        <label className="label">Cash Persentage</label>
                        <div className="field">
                            <div className="control">
                                <input className="input" type='number' placeholder="Product Price"
                                    {...edit("cash_p", {
                                        required: 'Cash Persentage is required',
                                        validate: {
                                            positive: v => parseInt(v) > 0 || 'Persentage should be greater than 0',
                                        }
                                    })} />
                                {editErrors.cash_p && <p className="help">{editErrors.cash_p.message}</p>}
                            </div>
                        </div>
                        <label className="label">Card Persentage</label>
                        <div className="field">
                            <div className="control">
                                <input className="input" type='number' placeholder="Product Price"
                                    {...edit("card_p", {
                                        required: 'Card Persentage is required',
                                        validate: {
                                            positive: v => parseInt(v) > 0 || 'Persentage should be greater than 0',
                                        }
                                    })} />
                                {editErrors.card_p && <p className="help">{editErrors.card_p.message}</p>}
                            </div>
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Edit Branch
                        </Button>
                        <Button variant="danger" onClick={() => { resetEdit(editTax) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            <Modal show={addTax} onHide={() => { setAddTax(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Tax</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleRegisterSubmit(onRegisterSubmit)} >
                <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...register("name", {
                                            required: 'Name is required',
                                        })} />
                                    {registerErrors.name && <p className="help">{registerErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                        </div>
                        <label className="label">Cash Persentage</label>
                        <div className="field">
                            <div className="control">
                                <input className="input" type='number' placeholder="Product Price"
                                    {...register("cash_p", {
                                        required: 'Cash Persentage is required',
                                        validate: {
                                            positive: v => parseInt(v) > 0 || 'Persentage should be greater than 0',
                                        }
                                    })} />
                                {registerErrors.cash_p && <p className="help">{registerErrors.cash_p.message}</p>}
                            </div>
                        </div>
                        <label className="label">Card Persentage</label>
                        <div className="field">
                            <div className="control">
                                <input className="input" type='number' placeholder="Product Price"
                                    {...register("card_p", {
                                        required: 'Card Persentage is required',
                                        validate: {
                                            positive: v => parseInt(v) > 0 || 'Persentage should be greater than 0',
                                        }
                                    })} />
                                {registerErrors.card_p && <p className="help">{registerErrors.card_p.message}</p>}
                            </div>
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Create Tax
                        </Button>
                        <Button variant="danger" onClick={() => { resetRegister() }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
};

export default Index;