import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_BRANCH, API_USER } from '../../constant/apiConstants';
import { toast } from 'react-toastify';
import CommonService from '../../constant/commonService';
import { Button, Modal } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux'
import jwt from 'jwt-decode'
import {
    useSearchParams,
} from "react-router-dom";
import { USER_TYPE } from '../../constant/common';

const Index = () => {
    const role = useSelector((state) => state.role.role)
    const token = useSelector((state) => state.token.token)
    const [editBranch, setEditBranch] = useState({})
    const { register: edit, handleSubmit: handleEditSubmit, reset: resetEdit, formState: { errors: editErrors } } = useForm({ mode: 'onChange', defaultValues: editBranch });
    const [branches, setBranches] = useState([])
    const { register, handleSubmit: handleRegisterSubmit, reset: resetRegister, formState: { errors: registerErrors } } = useForm({ mode: 'onChange' });
    const [addBranch, setAddBranch] = useState(false)
    const [showEditModel, setShowEditModel] = useState(false)
    const [reloadBranches, setReloadBranches] = useState(false)
    let [availableManagers, setAvailableManagers] = useState([])
    let [availableCashiers, setAvailableCashiers] = useState([])
    let [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        //API call
        let request = {};
        if (role.id == 1) {
            request = { ...API_BRANCH.fetchAvailableManagers };
            if (showEditModel) {
                request.obj = { _id: editBranch._id }
            }
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    setAvailableManagers(res.data)
                }
            })
        }
        request = { ...API_BRANCH.fetchAvailableCashiers };
        if (showEditModel) {
            request.obj = { _id: editBranch._id }
        }
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setAvailableCashiers(res.data)
            }
        })
    }, [showEditModel, addBranch])

    useEffect(() => {
        //API call
        setSearchParams({});
            let request =role.id == USER_TYPE.ADMIN ? { ...API_BRANCH.fetchAllBranches } :role.id == USER_TYPE.MANAGER ?
                { ...API_BRANCH.fetchAllBranches, url: API_BRANCH.fetchAllBranches.url + jwt(token).branchId } : null;
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    if (role.id == 1) {
                        setBranches(res.data)
                    }
                    else if (role.id == 3) {
                        setBranches(Array(res.data))
                    }
                }
            });
    }, [reloadBranches])

    const onEditSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_BRANCH.update, url: API_BRANCH.update.url + editBranch._id };
            request.obj = { ...data };
            delete request.obj._id
            UtilService.callApi(request, function (res) {
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadBranches(v => !v)
                    setShowEditModel(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const onRegisterSubmit = data => {
        if (data !== '') {
            //API call
            let request = { ...API_BRANCH.create };
            request.obj = { ...data };
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    resetRegister()
                    toast.success(res.msg)
                    setReloadBranches(v => !v)
                    setAddBranch(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const deleteBranch = (id, name) => {
        if (window.confirm(`Are you sure you want to Delete ${name} Branch ?`)) {
            let request = { ...API_BRANCH.delete, url: API_BRANCH.delete.url + id }
            UtilService.callApi(request, function (res) {
                if (res && res.data && res.status === true) {
                    toast.success(res.msg)
                    setReloadBranches(v => !v)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            })
        }
    }

    useEffect(() => {
        resetEdit(editBranch)
    }, [showEditModel, availableManagers, availableCashiers])


    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h2 className="title">
                        {role.id == 1 ? "All Branches" :role.id == USER_TYPE.MANAGER ? "Branch" : null}
                    </h2>
                    {role.id == 1 ?
                        <button className="button light" onClick={() => { setAddBranch(true) }}>
                            <span className="icon"><i className="mdi mdi-pencil"></i></span>
                            <span>Create Branch</span>
                        </button>
                        :role.id == USER_TYPE.MANAGER ? null : null}
                </div>
            </section>

            {role.id == 1 ?
                <section className="is-hero-bar">
                    <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                        <h5 className="title">
                            Search User By Name
                        </h5>
                        <div className="control icons-left icons-right">
                            <input className="input" type="text" name="email" value={searchParams.get("filter") || ""}
                                onChange={(event) => {
                                    let filter = event.target.value;
                                    if (filter) {
                                        setSearchParams({ filter });
                                    } else {
                                        setSearchParams({});
                                    }
                                }} />
                            <span className="icon is-small left"><i className="mdi mdi-magnify "></i></span>
                            {searchParams.get("filter") ? <span style={{ cursor: "pointer" }} onClick={() => { setSearchParams({}); }} className="icon is-small right"><i className="mdi mdi-close"></i></span> : null}
                        </div>
                    </div>
                </section>
                :role.id == USER_TYPE.MANAGER ? null : null}

            <div className="card has-table" style={{ padding: '15px' }}>
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Manager</th>
                                <th>Cashiers</th>
                                <th>Added By</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {branches
                                .filter((branch) => {
                                    let filter = searchParams.get("filter");
                                    if (!filter) return true;
                                    let name = branch.name.toLowerCase();
                                    return name.startsWith(filter.toLowerCase());
                                }).map((branch) => (
                                    <tr key={branch._id}>
                                        <td data-label="Name">{branch.name}</td>
                                        <td data-label="Company">{branch.address}</td>
                                        <td data-label="City">{branch.manager.name}</td>
                                        {branch.cashiers.length > 0 ? <td>{branch.cashiers.map((cashier, index, cashiers) => (<span key={index}>{`${cashier.name}${(index + 1 === cashiers.length) ? '' : ','}`}</span>))}</td> : <td>Not Assign Yet</td>}
                                        <td data-label="City">{branch.admin.name}</td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(branch.createdAt)}</small>
                                        </td>
                                        <td data-label="Created">
                                            <small className="text-gray-500" title="Oct 25, 2021">{CommonService.displayDate(branch.updatedAt)}</small>
                                        </td>
                                        <td className="actions-cell">
                                            <div className="buttons right nowrap">
                                                <button className="button small green --jb-modal" onClick={() => { setShowEditModel(true); setEditBranch({ _id: branch._id, name: branch.name, address: branch.address, managerId: branch.manager._id, cashiers: branch.cashiers.map((obj) => obj._id) }) }} type="button">
                                                    <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                                </button>
                                                {role.id == 1 ?
                                                    <button className="button small red --jb-modal" data-target="sample-modal" onClick={() => { deleteBranch(branch._id, branch.name) }} type="button">
                                                        <span className="icon"><i className="mdi mdi-trash-can"></i></span>
                                                    </button>
                                                    : null}
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <Modal show={showEditModel} onHide={() => { setShowEditModel(false); }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Manager</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...edit("name", {
                                            required: 'Name is required',
                                        })} />
                                    {editErrors.name && <p className="help">{editErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Address</label>
                            <div className="field">
                                <div className="control">
                                    <textarea className="input" placeholder="Branch Address"
                                        {...edit("address", {
                                            required: 'Address is required',
                                        })} />
                                    {editErrors.address && <p className="help">{editErrors.address.message}</p>}
                                </div>
                            </div>
                        </div>
                    </div>
                    {role.id == 1 ?
                        <div className="field">
                            <label className="label">Select Manager</label>
                            <div className="control">
                                <div className="select">
                                    <select {...edit("managerId")}>
                                        {availableManagers.map((manager) => (
                                            <option key={manager._id} value={manager._id}>{manager.name}</option>
                                        ))}
                                    </select>
                                </div>
                                {editErrors.managerId && <p className="help">{editErrors.managerId.message}</p>}
                            </div>
                        </div>
                        : null}
                    <div className="field">
                        <label className="label">Select Cashier</label>
                        <div className="control">
                            <div className="select">
                                <select {...edit("cashiers")} multiple>
                                    {availableCashiers.map((cashier) => (
                                        <option key={cashier._id} value={cashier._id}>{cashier.name}</option>
                                    ))}
                                </select>
                            </div>
                            {editErrors.cashiers && <p className="help">{editErrors.cashiers.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Edit Branch
                        </Button>
                        <Button variant="danger" onClick={() => { resetEdit(editBranch) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

            <Modal show={addBranch} onHide={() => { setAddBranch(false) }}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Branch</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleRegisterSubmit(onRegisterSubmit)} >
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control icons-left">
                                    <input className="input" type="text" placeholder="Name"
                                        {...register("name", {
                                            required: 'Name is required',
                                        })} />
                                    {registerErrors.name && <p className="help">{registerErrors.name.message}</p>}
                                    <span className="icon left"><i className="mdi mdi-account"></i></span>
                                </div>
                            </div>
                            <label className="label">Address</label>
                            <div className="field">
                                <div className="control">
                                    <textarea className="input" placeholder="Branch Address"
                                        {...register("address", {
                                            required: 'Address is required',
                                        })} />
                                    {registerErrors.address && <p className="help">{registerErrors.address.message}</p>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Manager</label>
                        <div className="control">
                            <div className="select">
                                <select {...register("managerId", {
                                    required: 'Manager is required',
                                })}>
                                    {availableManagers.map((manager) => (
                                        <option key={manager._id} value={manager._id}>{manager.name}</option>
                                    ))}
                                </select>
                            </div>
                            {registerErrors.managerId && <p className="help">{registerErrors.managerId.message}</p>}
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Select Cashier</label>
                        <div className="control">
                            <div className="select">
                                <select {...register("cashiers", {
                                    required: 'Cashier is required',
                                })} multiple>
                                    {availableCashiers.map((cashier) => (
                                        <option key={cashier._id} value={cashier._id}>{cashier.name}</option>
                                    ))}
                                </select>
                            </div>
                            {registerErrors.cashiers && <p className="help">{registerErrors.cashiers.message}</p>}
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Create Branch
                        </Button>
                        <Button variant="danger" onClick={() => { resetRegister() }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
};

export default Index;