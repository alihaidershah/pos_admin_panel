import { useForm } from 'react-hook-form';
import { API_USER } from '../constant/apiConstants';
import { toast } from 'react-toastify';
import { useNavigate } from "react-router-dom";
// import { API_STATUS, DEFAULT_API_ERROR } from '../constant/common';
import { useDispatch } from 'react-redux'
import { setRole } from '../features/userRole/userRoleSlice'
import { setToken } from '../features/token/tokenSlice'
import UtilService from '../services/common.service';
import { USER_TYPE } from '../constant/common';
import { useState } from 'react';

function SignIn(props) {
  const dispatch = useDispatch()
  const { register, handleSubmit, formState: { errors } } = useForm({ mode: 'onChange' });
  const [showPassword, setShowPassword] = useState(false)
  let navigate = useNavigate();
  const onSubmit = data => {
    if (data !== '') {
      //API call
      let request = { ...API_USER.login };
      request.obj = { ...data };
      UtilService.callApi(request, function (res) {
        if (res && res.data && res.status === true) {
          // localStorage.setItem("token", JSON.stringify(res.data.jwt))
          localStorage.setItem("user", JSON.stringify(res.data.user))
          dispatch(setRole(res.data.user.role))
          dispatch(setToken(res.data.jwt))
          if (res.data.user.role.id === USER_TYPE.ADMIN) {
            navigate("/dashboard", { replace: true });
          }
          if (res.data.user.role.id === USER_TYPE.MANAGER) {
            navigate("/users", { replace: true });
          }
          if (res.data.user.role.id === USER_TYPE.CASHIER) {
            navigate("/order", { replace: true });
          }
          toast.success(res.msg)
        }
        else {
          toast.error((res.response.data.msg))
        }
      });
    } else {
      console.log("errors")
    }
  };

  return (
    <>
      <section className="section main-section">
        <div className="card" style={{ marginRight: "20rem", marginLeft: "10rem" }}>
          <header className="card-header">
            <p className="card-header-title">
              <span className="icon"><i className="mdi mdi-lock"></i></span>
              Login
            </p>
          </header>
          <div className="card-content">
            <form onSubmit={handleSubmit(onSubmit)}>

              <div className="field spaced">
                <label className="label">Email</label>
                <div className="control icons-left">
                  <input className="input" type="text" placeholder="Enter Your Email" name="email" onKeyUp={(event) => {
                    let k = event ? event.which : window.event.keyCode;
                    if (k === 32) return false;
                  }}
                    {...register("email", {
                      required: 'Email is required',
                      pattern: {
                        value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'Please enter a valid email',
                      },
                    })} />
                  {errors.email && <p className="help">{errors.email.message}</p>}
                  <span className="icon is-small left"><i className="mdi mdi-account"></i></span>
                </div>
              </div>

              <label className="label">Password</label>
              <div className="field">
                <div className="control icons-left icons-right">
                  <input className="input" type={showPassword ? 'text' : 'password'} placeholder="Enter Your Password"
                    {...register("password", {
                      required: { value: true, message: 'Password is required' },
                      minLength: {
                        value: 5,
                        message: "Password Must Be Greater Than Five Character"
                      }
                    })} />
                  {errors.password && <p className="help">{errors.password.message}</p>}
                  <span className="icon left"><i className="mdi mdi-lock"></i></span>
                  <span className="icon right" onClick={() => { setShowPassword(value => !value) }}><i className={showPassword ? "mdi mdi-eye-outline " : "mdi mdi-eye-off-outline "}></i></span>
                </div>
              </div>
              <div className="control">
                <button type="submit" className="button blue">
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>

      </section >

    </>
  );
}

export default SignIn;
