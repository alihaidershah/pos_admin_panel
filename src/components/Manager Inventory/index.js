import React, { useEffect, useState } from 'react';
import UtilService from '../../services/common.service';
import { API_BRANCH, API_PRODUCT } from '../../constant/apiConstants';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux'
import jwt from 'jwt-decode'
import {
    useSearchParams,
} from "react-router-dom";
import { USER_TYPE } from '../../constant/common';


const Index = () => {
    const role = useSelector((state) => state.role.role)
    const token = useSelector((state) => state.token.token)
    const [editQty, setEditQty] = useState({})
    const { register: edit, handleSubmit: handleEditSubmit, reset: resetEdit, formState: { errors: editErrors } } = useForm({ mode: 'onChange', defaultValues: editQty });
    const [branches, setBranches] = useState([])
    const [products, setProducts] = useState([])
    const [reloadProducts, setReloadProducts] = useState(false)
    const [productQty, setProductQty] = useState([])
    const [showEditModel, setShowEditModel] = useState(false)
    let [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        resetEdit(editQty)
    }, [showEditModel])


    useEffect(() => {
        //API call
        setSearchParams({});
        let request = role.id == USER_TYPE.ADMIN ? { ...API_BRANCH.fetchAllBranches } : role.id == USER_TYPE.MANAGER ?
            { ...API_BRANCH.fetchAllBranches, url: API_BRANCH.fetchAllBranches.url + jwt(token).branchId } : null;
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                if (role.id == USER_TYPE.ADMIN) {
                    setBranches(res.data)
                }
                else if (role.id == USER_TYPE.MANAGER) {
                    setBranches(Array(res.data))
                }
                let element = document.querySelectorAll(".branchesArray")
                element.forEach((item) => {
                    item.selectedIndex = 0
                });
            }
        });
        request = { ...API_PRODUCT.fetchAllProducts };
        UtilService.callApi(request, function (res) {
            if (res && res.data && res.status === true) {
                setProducts(res.data)
            }
        });
    }, [reloadProducts])

    useEffect(() => {
        let productQuantity = []
        if (products.length > 0 && branches.length > 0) {
            products.forEach((p, index) => {
                let inventory = branches[0].inventory
                productQuantity.push({ p_id: p._id, qty: inventory[index].quantity })
            })
            setProductQty(productQuantity)
        }
        // console.log(productQty)
    }, [products])

    const onEditSubmit = data => {
        let { p_id: productId, b_id: branchId, qty: quantity } = data
        if (data !== '') {
            //API call
            let request = { ...API_BRANCH.updateInventory };
            if (typeof (branchId) == 'undefined') {
                branchId = branches[0]._id
            }
            request.obj = { productId, branchId, quantity };
            UtilService.callApi(request, function (res) {
                if (res && res.status === true) {
                    toast.success(res.msg)
                    setReloadProducts(v => !v)
                    setShowEditModel(false)
                }
                else {
                    //   console.log(res.response.data.msg)
                    toast.error((res.response.data.msg))
                }
            });
        } else {
            console.log("errors")
        }
    };

    const changeQuantity = (branchId, productId) => {
        let selectBranch = branches.filter((branch) => { return branch._id == branchId })
        let quantity = selectBranch[0].inventory.filter((obj) => { return obj.product == productId })[0].quantity
        let productQuantities = [...productQty]
        let p = productQuantities.filter((obj) => { return obj.p_id == productId })
        p[0].b_id = branchId; p[0].qty = quantity;
        setProductQty(productQuantities)

    }
    return (
        <>
            <section className="is-hero-bar" style={{ paddingTop: "10px" }}>
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h2 className="title">
                        Manage Inventory
                    </h2>
                </div>
            </section>
            <section className="is-hero-bar">
                <div className="flex flex-col md:flex-row items-center justify-between space-y-6 md:space-y-0">
                    <h5 className="title">
                        Search By Product Name
                    </h5>
                    <div className="control icons-left icons-right">
                        <input className="input" type="text" name="email" value={searchParams.get("filter") || ""}
                            onChange={(event) => {
                                let filter = event.target.value;
                                if (filter) {
                                    setSearchParams({ filter });
                                } else {
                                    setSearchParams({});
                                }
                            }} />
                        <span className="icon is-small left"><i className="mdi mdi-magnify "></i></span>
                        {searchParams.get("filter") ? <span style={{ cursor: "pointer" }} onClick={() => { setSearchParams({}); }} className="icon is-small right"><i className="mdi mdi-close"></i></span> : null}
                    </div>
                </div>
            </section>

            <div className="card has-table" style={{ padding: '15px' }}>
                <div className="card-content">
                    <table>
                        <thead>
                            <tr>
                                <th>Products</th>
                                <th>Branches</th>
                                <th>Qunatity</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products
                                .filter((product) => {
                                    let filter = searchParams.get("filter");
                                    if (!filter) return true;
                                    let name = product.name.toLowerCase();
                                    return name.startsWith(filter.toLowerCase());
                                })
                                .filter((product) => {
                                    return product.status == "ACTIVE";
                                }).map((product) => (
                                    <tr key={product._id}>
                                        <td data-label="Name">{product.name}</td>
                                        <td data-label="branches">
                                            {branches.length > 0 ?
                                                <select disabled={role.id == USER_TYPE.ADMIN ? false : role.id == USER_TYPE.MANAGER ? true : null} className='branchesArray' onChange={(e) => { changeQuantity(e.target.value, product._id) }}>
                                                    {branches.map((branch, index) => (
                                                        <option key={branch._id} value={branch._id}>{branch.name}</option>
                                                    ))}
                                                </select>
                                                : <span>No Branch Exist.</span>}
                                        </td>
                                        <td>{productQty.length > 0 ? (productQty.filter((obj) => { return obj.p_id == product._id }))[0].qty : 0}</td>
                                        <td className="actions-cell">
                                            {branches.length > 0 ?
                                                <div className="buttons nowrap">
                                                    <button className="button small green --jb-modal" onClick={() => { setShowEditModel(true); let obj = productQty.filter((p) => { return p.p_id == product._id }); setEditQty({ ...obj[0] }) }} type="button">
                                                        <span className="icon"><i className="mdi mdi-pencil"></i></span>
                                                    </button>
                                                </div>
                                                : null}
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>

            <Modal show={showEditModel} onHide={() => { setShowEditModel(false); }}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Manager</Modal.Title>
                </Modal.Header>
                <form style={{ padding: "10px" }} onSubmit={handleEditSubmit(onEditSubmit)} >
                    <div className="field">
                        <label className="label">Update Quantity</label>
                        <div className="field-body">
                            <div className="field">
                                <div className="control">
                                    <input className="input" type="text" placeholder="Name"
                                        {...edit("qty", {
                                            required: 'Qty is required',
                                        })} />
                                    {editErrors.qty && <p className="help">{editErrors.qty.message}</p>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <Modal.Footer>
                        <Button variant="primary" type="submit">
                            Edit Quantity
                        </Button>
                        <Button variant="danger" onClick={() => { resetEdit(editQty) }}>
                            Reset
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

        </>
    );
};

export default Index;