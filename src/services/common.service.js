import axios from 'axios';
import moment from 'moment';
// import { Refresh_Token } from '../constant/apiConstants';
import { API_URL } from '../constant/common';
import {store} from '../store/store'
import { showLoader,hideLoader } from '../features/loader/loaderSlice'


const apiInstance = axios.create({
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json',
    }
});

/**
 * Common Request Middleware for AJAX call
 * config - Headers, Loaders
 */
apiInstance.interceptors.request.use(
    function (config) {
        const header_token = store.getState().token && store.getState().token.token;
        config.headers.auth_token = header_token
        return config
    },
    function (error) {
        return Promise.reject(error)
    }
)
/**
 * Common Response Middleware for AJAX call
 * config - Headers, Loaders
 */
apiInstance.interceptors.response.use(
    function (response) {
        return response
    },
    function (error) {

        return Promise.reject(error)
    }
)

const UtilService = {
    callApi(object, cb, isRefreshToken = false) {
        let headers = {
            headers: {
                'Content-Type': 'application/json',
                showLoader: object.showLoader ? object.showLoader : undefined
            }
        };
        if (object.showLoader) {
            store.dispatch(showLoader())
        }
        if (object.method === "get") {
            object.url = object.url + "?t=" + moment().millisecond()
        }
        apiInstance[object.method](object.url, object.obj, headers)
            .then(({ data }) => {
                if (object.showLoader) {
                    store.dispatch(hideLoader())
                }
                cb(data);
            })
            .catch(error => {
                console.log("here is error",error)
                if (object.showLoader) {
                    store.dispatch(hideLoader())
                }
                if (error) cb(error);
            });
    }
};

export default UtilService;
