import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  status: false,
}

export const loaderSlice = createSlice({
  name: 'loader',
  initialState,
  reducers: {
    showLoader: (state) => {
      state.status=true
    },
    hideLoader: (state) => {
      state.status=false
    },
  },
})


export const { showLoader,hideLoader } = loaderSlice.actions

export default loaderSlice.reducer