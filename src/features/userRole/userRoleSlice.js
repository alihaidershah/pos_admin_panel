import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  role: {}
}

export const userRoleSlice = createSlice({
  name: 'role',
  initialState,
  reducers: {
    setRole: (state, action) => {
      state.role = action.payload
    },
  },
})


export const { setRole } = userRoleSlice.actions

export default userRoleSlice.reducer