import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navigate, BrowserRouter, Routes, Route, useLocation } from 'react-router-dom';
import SignIn from './components/SignIn'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import App from './components/App'
import Users from './components/Manager Users';
import Branches from './components/Manager Branches/index'
import Product from './components/Manager Products/index'
import Inventory from './components/Manager Inventory/index'
import Order from './components/Manager Order/index'
import Tax from './components/Manager Taxes/index'
import Dashboard from "./components/Dashboard";
import { persistor, store } from './store/store'
import { Provider, useSelector } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import jwt from 'jwt-decode'
import Navbar from './components/Navbar/Navbar';
import { USER_TYPE } from './constant/common';
// import { USER_TYPE } from './constant/common'
// import CreateUser from './components/Manager Users/CreateUser';

function Root() {
  const token = useSelector((state) => state.token.token)
  const role = useSelector((state) => state.role.role)

  const ProtectedRoute = ({ children }) => {
    let location = useLocation().pathname;
    let Permissions = {
      "ADMIN": ['/dashboard', '/users', '/branches', '/inventory', '/order', '/products', '/tax'],
      "MANAGER": ['/users', '/branches', '/inventory', '/order', '/tax'],
      "CASHIER": ['/order']
    }
    if (token) {
      if (location === '/' && role.id === USER_TYPE.ADMIN) {
        return <Navigate to="/dashboard" replace />
      }
      else if (location === '/' && role.id === USER_TYPE.MANAGER) {
        return <Navigate to="/users" replace />
      }
      else if (location === '/' && role.id === USER_TYPE.CASHIER) {
        return <Navigate to="/order" replace />
      }
      else if (Permissions[jwt(token).role.name].includes(location)) {
        return children;
      }
      else {
        return (
          <>
            <Navbar />
            <div style={{ display: 'flex', justifyContent: 'center', color: 'red', fontSize: '30px', textTransform: 'uppercase' }}>
              You Have No Access This Route</div>
          </>
        )
      }
    }
    else {
      return <Navigate to="/login" replace />;
    }

  };
  return (
    <Routes>
      <Route path="/login" element={<SignIn />} />
      {/* <Route path="/" element={<Navigate to="/dashboard" />} /> */}
      <Route path="/" element={<ProtectedRoute><App /></ProtectedRoute>} >
        <Route path="/dashboard" element={<ProtectedRoute><Dashboard /></ProtectedRoute>} />
        <Route path="/users" element={<ProtectedRoute><Users /></ProtectedRoute>} />
        <Route path="/branches" element={<ProtectedRoute><Branches /></ProtectedRoute>} />
        <Route path="/inventory" element={<ProtectedRoute><Inventory /></ProtectedRoute>} />
        <Route path="/order" element={<ProtectedRoute><Order /></ProtectedRoute>} />
        <Route path="/products" element={<ProtectedRoute><Product /></ProtectedRoute>} />
        <Route path="/tax" element={<ProtectedRoute><Tax /></ProtectedRoute>} />
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Route>
    </Routes>
  );
}



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<>
  <BrowserRouter>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Root />
      </PersistGate>
    </Provider>
  </BrowserRouter>
  <ToastContainer></ToastContainer>
</>);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
